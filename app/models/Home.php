<?php

use Clearweb\FrontPages\Page\Page;
use Clearweb\FrontPages\Layout\OneColumnLayout;

class Home extends Page
{
    public function __construct()
    {
        $this->setSlug('home');
    }
    
    public function init()
    {
        $homeTitle = Settings::get('site-title', 'set title!');
        if (Settings::get('site-slogan', false)) {
            $homeTitle.= ' - '.Settings::get('site-slogan', '');
        }
        
        $this->setTitle($homeTitle)
            ->setLayout(new OneColumnLayout)
            ;
        
        return parent::init();
    }
}