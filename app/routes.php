<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


/*************************************
 * Admin
 */

$admin = Clearworks::getEnvironment('admin');

// add the show file page...
$admin->addPage(new \Clearweb\Clearwebapps\File\ShowFilePage);

// set the settings page as the default page for the admin
$admin->addPage(new \Clearweb\Clearwebapps\Setting\SettingsPage, true);

// populate the menu
Event::listen('env.init', function() {
    Menu::addLink(trans('clearwebapps::settings.settings'), Clearworks::getPageUrl(new \Clearweb\Clearwebapps\Setting\SettingsPage));
});






/*************************************
 * Front end
 */


$front = Clearworks::getEnvironment('public');
// add the show file page...
$front->addPage(new \Clearweb\Clearwebapps\File\ShowFilePage);

// add the empty home page
$front->addPage(new Home, true);


